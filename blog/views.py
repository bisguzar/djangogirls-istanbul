from django.shortcuts import render, redirect
from .models import Post, Comment
from django.utils import timezone
from django.contrib import messages
from .forms import CommentForm


def post_list(request):

    posts = Post.objects.all().order_by('published_date')
    return render(request, 'blog/post_list.html', {'title':'Django Girls Blog', 'posts':posts})


def post_detail(request, pk):
    try:
        post = Post.objects.get(pk=pk)

    except:
        messages.add_message(request, messages.WARNING, 'bir hata oldu')
        return redirect('post_list')

    try:
        comments = Comment.objects.filter(post=post)
    except:
        comments = []

    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = Post.objects.get(pk=pk)
            comment.save()

            return render(request, 'blog/post_detail.html', {'title': post.title, 'post': post,
                                                             'form': CommentForm,
                                                             'comments': comments})

    else:

        return render(request, 'blog/post_detail.html', {'title': post.title, 'post': post,
                                                     'form': CommentForm,
                                                    'comments': comments})
